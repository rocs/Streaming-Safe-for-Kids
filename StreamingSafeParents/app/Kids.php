<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kids extends Model
{

  protected $table = "childrens";
  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
      'user_id','username','password','birthday','search','restricted_mode',
  ];
}
