<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Kids;
use Auth;

class KidsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $kids=Kids::where('user_id', '=', Auth::id())->get();
      //dd($kids);
      return view('kids/index', compact('kids'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('kids/creat');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $this->validate($request, [
          'username'=>'required',
          'password' => 'required',
          'birthday' => 'required',
          'search' => 'required',
          'restricted_mode' => 'required',
      ]);

      $kid = new Kids();
      $kid->username = $request->input("username");
      $kid->password =  bcrypt($request->input("password"));
      $kid->user_id = Auth::id();
      $kid->birthdate = $request->input("birthday");
      $kid->search = $request->input("search");
      $kid->restricted_mode = $request->input("restricted_mode");
      if ($kid->save()){
        return redirect('/ninos');
      }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      $kid = Kids::find($id);
      return view('kids/show', compact('kid'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $kid = Kids::find($id);
      return view('kids/update', compact('kid'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $this->validate($request, [
          'username' => 'required',
          'password' => 'required',
          'birthday' => 'required',
          'search' => 'required',
          'restricted_mode' => 'required',
      ]);
      $kid = Kids::find($id);
      $kid->username = $request->input("username");
      $kid->password = bcrypt($request->input("password"));
      $kid->birthdate = $request->input("birthday");
      $kid->search = $request->input("search");
      $kid->restricted_mode = $request->input("restricted_mode");
      if ($kid->save()){
        return redirect('/ninos');
      }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $kid = Kids::find($id);

      $kid->delete();
      return redirect('/ninos');
    }
}
