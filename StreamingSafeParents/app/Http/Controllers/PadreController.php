<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Auth;
class PadreController extends Controller
{
  public function __construct()
  {
      $this->middleware('auth');
  }
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
      $users = User::all();
      return view('padre/index', compact('users'));
  }

  public  function cambio($id, $rol){
      if ($rol === 'C') {
        $cambio = 'P';
      }else{
        $cambio = 'C';
      }
      $user = User::find($id);
      $user->rol = $cambio;
      if ($user->save()){
        if ($id == Auth::id()) {
          return redirect('home');

        }
        return redirect('padres');
      }
  }
}
