<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Category;
use Auth;
class CategoryController extends Controller
{
  public function __construct()
  {
      $this->middleware('auth');
  }
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    $categories = Category::all();
    return view('categories/index', compact('categories'));
  }
  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
      return view('categories/create');
  }
  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    //dd('hola');
    $this->validate($request, [
        'name' => 'required',
        'description' => 'required',
        'mimimum_age' => 'required',
    ]);
    $category = new Category();
    $category->name = $request->input("name");
    $category->description = $request->input("description");
    $category->mimimum_age = $request->input("mimimum_age");
    //dd($category->mimimum_age);
    if ($category->save()){
      return redirect('/categories');
    }
  }
  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
    $category = Category::find($id);
    return view('categories/show', compact('category'));
  }
  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function edit($id)
  {
    $category = Category::find($id);
    return view('categories/update', compact('category'));
  }
  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id)
  {
    $this->validate($request, [
        'name' => 'required',
        'description' => 'required',
        'mimimum_age' => 'required',
    ]);
    $category = Category::find($id);
    $category->name = $request->input("name");
    $category->description = $request->input("description");
    $category->mimimum_age = $request->input("mimimum_age");
    if ($category->save()){
      return redirect('/categories');
    }
  }
  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
    $category = Category::find($id);
    $category->delete();
    return redirect('/categories');
  }
}
