<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Video;
use App\Playlist;
use App\ListItems;
use App\User;
use Auth;

class PlaylistController extends Controller
{
  public function __construct()
  {
      $this->middleware('auth');
  }
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    $playlists = Playlist::where('user_id','=',Auth::id())->orWhere('public','=','Y')->get();
    return view('playlists/index', compact('playlists'));
  }
  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
      return view('playlists/create');
  }
  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    //dd('hola');
    $this->validate($request, [
        'description' => 'required',
        'public' => 'required',
    ]);
    $playlist = new Playlist();
    $playlist->user_id = Auth::user()->id;
    $playlist->description = $request->input("description");
    $playlist->public = $request->input("public");
    if ($playlist->save()){
      return redirect('/list');
    }
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function add($id)
  {
      $video = Video::find($id);
      $playlists = Playlist::where('user_id','=',Auth::id())->orWhere('public','=','Y')->get();
      $datos = array($video , $playlists);
      return view('playlists/add', compact('datos'));
  }
/**
  **
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function play(Request $request)
  {
    $this->validate($request, [
        'playlist_id' => 'required',
        'video_id' => 'required',
    ]);
    $listItems = new ListItems();
    $listItems->playlist_id = $request->input("playlist_id");
    $listItems->video_id = $request->input("video_id");
    if ($listItems->save()){
      return redirect('/video/show/'.$request->input("video_id"));
    }
  }
  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
    $playlist = Playlist::find($id);
    $user = User::find($playlist->user_id);
    $listItems = ListItems::where('playlist_id','=',$id)->get();
    $video = Video::all();
    $datos = array($playlist, $user, $listItems, $video);
    return view('playlists/show', compact('datos'));
  }
  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function edit($id)
  {
    $playlist = Playlist::find($id);
    return view('playlists/update', compact('playlist'));
  }
  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id)
  {
    $this->validate($request, [
      'user_id' => 'required',
      'description' => 'required',
      'public' => 'required',
  ]);
    $playlist = Playlist::find($id);
    $playlist->user_id = $request->input("user_id");
    $playlist->description = $request->input("description");
    $playlist->public = $request->input("public");
    if ($playlist->save()){
      return redirect('/list');
    }
  }
  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
    $playlist = Playlist::find($id);
    $playlist->delete();
    return redirect('/list');
  }

  public function delete($id_video, $id_play)
  {
    $listItems = ListItems::where('playlist_id','=',$id_play,"and","video_id","=",$id_video)->first();
    //dd($listItems);
    $listItems->delete();
    return redirect('/list/show/'.$id_play);
  }
}
