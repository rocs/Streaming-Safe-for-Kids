<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Tag;
use App\Video;
use App\TagVideo;
use Auth;

class TagVideoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
      $tags = Tag::all();
      $datos = array($tags, $id );
      return view('tag/list', compact('datos'));
    }
    public function video($id){
      $tags = Tag::all();
      $tagsvideo = TagVideo::where('video_id','=',$id)->get();

      $filter = array();
      foreach ($tags as $tag) {
        $esta = 1;
        //echo $tag->id."tag->id </br>";
        foreach ($tagsvideo as $tagvideo) {
          //echo $tagvideo->tag_id."tagvideo->tag_id </br>";

          if ($tag->id === $tagvideo->tag_id) {
            $esta = 0;
            //echo "$esta esta</br>";
          }
        }
        if ($esta == 1) {
          //echo $tag."tag</br>";
          $filter[] = ($tag);
        }
        //echo "</br></br>";
      }
      $datos = array($filter, $id );
      //dd($filter);
      return view('tag/listAdd', compact('datos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      $arrayName = array( );
      for ($i=1; $i <= count($_POST); $i++) {
        if (isset($_POST[$i])) {
            $taglist = new TagVideo;
            $taglist->video_id = $_POST['id'];
            $taglist->tag_id = $_POST[$i];
            $taglist->save();
        }
      }
      return redirect('video');
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      $tagsVideo = TagVideo::where('video_id','=',$id)->get();
      $datos = array();
      foreach ($tagsVideo as $tagVideo) {
        $tag = Tag::find($tagVideo->tag_id);
        $datos[] = ($tag);
      }
      $cosas = array($datos, $id);
      return view('tag/showVideo', compact('cosas'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, $idtag)
    {
      $videoTags = TagVideo::where('video_id','=',$id)->get();
      //dd($videoTags);
      foreach ($videoTags as $videoTag) {
        if ($videoTag->tag_id == $idtag) {
          //dd($videoTag);
          $videoTag->delete();
          return redirect('taglist/show/'.$id);
        }
      }

    }
}
