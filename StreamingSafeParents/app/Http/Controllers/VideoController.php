<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Video;
use App\Category;
use Auth;
class VideoController extends Controller
{
  public function __construct()
  {
      $this->middleware('auth');
  }
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    $video = Video::all();
    return view('videos/index', compact('video'));
  }
  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
    $categories = Category::all();
      return view('videos/create', compact('categories'));
  }
  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    //dd('hola');
    $this->validate($request, [
        'category_id' => 'required',
        'description' => 'required',
        'url' => 'required',
    ]);
    $video = new Video();
    $video->category_id = $request->input("category_id");
    $video->description = $request->input("description");
    $video->url = $request->input("url");
    if ($video->save()){
      return redirect('taglist/register/'.$video->id);
    }
  }
  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
    $video = Video::find($id);
    $categoria = Category::find($video->category_id);
    $datos = array($video, $categoria);
    return view('videos/show', compact('datos'));
  }
  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function edit($id)
  {
    $video = Video::find($id);
    $categoria = Category::find($video->category_id);
    $categorias = Category::all();
    $datos = array($video, $categoria,$categorias );
    return view('videos/update', compact('datos'));
  }
  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id)
  {
    $this->validate($request, [
      'category_id' => 'required',
      'description' => 'required',
      'url' => 'required',
  ]);
    $video = Video::find($id);
    $video->category_id = $request->input("category_id");
    $video->description = $request->input("description");
    $video->url = $request->input("url");
    if ($video->save()){
      return redirect('/video');
    }
  }
  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
    //dd($id);
    $video = Video::find($id);
    $video->delete();
    return redirect('/video');
  }
}
