<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ChildrenPlaylist;
use App\Kids;
use App\Playlist;
use Auth;
class ChildrenPlayListController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
      //obtiene los kids con el id del padre
      $kids=Kids::find($id);
      $plays = Playlist::where('user_id','=',Auth::id())->orWhere('public','=','Y')->get();
      $r = ChildrenPlaylist::where('children_id','=',$id)->get();
      $arrayName = array();
      $t = true;
      foreach ($plays as $play) {
        foreach ($r as $k) {
            if($k->playlist_id == $play->id){
              $t = false;
            }
        }
        if ($t) {
          $arrayName[] = ($play);
        }
        $t = true;
      }
      $datos = array($kids, $arrayName );
      return view('childrenPlay/list', compact('datos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
            $kid = new ChildrenPlaylist;
            $kid->playlist_id = $_POST['playlist'];
            $kid->children_id = $_POST['id'];
            if($kid->save()){
              return redirect('/ninos');
            }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $arrayName = array( );
      for ($i=1; $i <= count($_POST); $i++) {
        if (isset($_POST[$i])) {
            $taglist = new ChildrenPlaylist;
            $taglist->playlist_id = $_POST['playlist'];
            $taglist->children_id = $_POST[$i];
            $taglist->save();
        }
      }
      return redirect('list/show/' . $_POST['playlist']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      //trae toda la lista de play list con ese kid
      $plays = ChildrenPlaylist::where('children_id','=',$id)->get();
      $datos = array();
      foreach ($plays as $play) {
        $play = Playlist::find($play->playlist_id);
        $datos[] = ($play);
      }
      $cosas = array($datos , $id);
      return view('childrenPlay/show', compact('cosas'));
    }
    public function kid($id){
      $play = Playlist::find($id);
      $kids = Kids::where('user_id', '=', Auth::id())->get();
      $h = ChildrenPlaylist::where('playlist_id', '=', $id)->get();
      $t = true;
      $arrayName = array();
      foreach ($kids as $kid) {
        foreach ($h as $k) {
            if($k->children_id == $kid->id){
              $t = false;
            }
        }
        if ($t) {
          $arrayName[] = ($kid);
        }
        $t = true;
      }
      $datos = array($play ,$arrayName );
      return view('playlists/listK', compact('datos'));
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, $playlistid)
    {
      $childrenPlaylists = ChildrenPlaylist::where('children_id','=',$id)->get();
      //dd($childrenPlaylists);

      foreach ($childrenPlaylists as $childrenPlaylist) {
        //dd($childrenPlaylist);
        if ($childrenPlaylist->playlist_id == $playlistid) {
          $childrenPlaylist->delete();
          return redirect('Children/show/'.$id);
        }
      }
    }
}
