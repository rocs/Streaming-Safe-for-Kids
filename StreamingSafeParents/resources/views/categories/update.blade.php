@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Editar Categoria</div>
                    <div class="panel-body">
                        <form class="form-horizontal" role="form" method="POST"  action="{{ url('/category/update', $category->id) }}">
                            {{ csrf_field() }}
                              <input type="hidden" name="_method" value="PATCH">
                            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                <label for="name" class="col-md-4 control-label">Nombre</label>
                                <div class="col-md-6">
                                    <input id="name" type="text" class="form-control" name="name" value="{{ $category->name}}" required autofocus>
                                    @if ($errors->has('name'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                                <label for="description" class="col-md-4 control-label">Descricion</label>
                                <div class="col-md-6">
                                    <input id="description" type="text" class="form-control" name="description" value="{{ $category->description }}" required>
                                    @if ($errors->has('description'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('description') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('mimimum_age') ? ' has-error' : '' }}">
                                <label for="mimimum_age" class="col-md-4 control-label">Edad Minima</label>
                                <div class="col-md-6">
                                    <input id="mimimum_age" type="number" class="form-control" min="0" max="17" name="mimimum_age" value="{{ $category->mimimum_age }}" required>
                                    @if ($errors->has('mimimum_age'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('mimimum_age') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">
                                        Editar
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
