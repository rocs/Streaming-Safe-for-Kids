@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
          <div class="panel panel-default">
                <div class="panel-body">
                  <a href="{{ url('category/register') }}"><button type="button" class="btn btn-success" name="button">Crear Categoria</button></a>
                  <br>
                  @if(count($categories)>0)
                  <div class="table-responsive">
                    <table class = "table table-hover">
                       <caption>Categorias</caption>
                       <thead>
                          <tr>
                             <th>Nombre</th>
                             <th>Descripcion</th>
                             <th>Accion</th>
                          </tr>
                       </thead>
                       <tbody>
                         @foreach ($categories as $category)
                           <tr>
                              <td>{{$category->name}}</td>
                              <td>{{$category->description}}</td>
                              <td><a class="btn btn-primary" href="category/show/{!! $category->id !!}">Ver</a>
                                <a class="btn btn-warning" href="category/edit/{!! $category->id !!}">Editar</a>
                                <a class="btn btn-danger" href="category/destroy/{!! $category->id !!}">Eliminar</a></td>
                           </tr>
                         @endforeach
                       </tbody>
                       @else
                           <h2>No hay Categorias registrados</h2>
                       @endif
                    </table>
                  </div>
                </div>
                </div>
              </div>
            </div>
          </div>
@endsection
