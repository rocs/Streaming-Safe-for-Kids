@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
          <div class="panel panel-default">
                <div class="panel-body">
                  <a href="{{ url('video/register') }}"><button type="button" class="btn btn-success" name="button">Agregar Video</button></a>
                  <br>
                  @if(count($video)>0)
                  <div class="table-responsive">
                    <table class = "table table-hover">
                       <caption>Videos</caption>
                       <thead>
                          <tr>
                             <th>Videos</th>
                             <th>Tags</th>
                             <th>Accion</th>
                          </tr>
                       </thead>
                       <tbody>
                         @foreach ($video as $videos)
                           <tr>
                              <td>{{$videos->description}}</td>
                              <td><a class="btn btn-primary" href="taglist/show/{!! $videos->id !!}">Etiquetas</a> </td>
                              <td><a class="btn btn-primary" href="video/show/{!! $videos->id !!}">Ver</a>
                                <?php if (Auth::user()->rol == "C"): ?>
                                  <a class="btn btn-warning" href="video/edit/{!! $videos->id !!}">Editar</a>
                                  <a class="btn btn-danger" href="video/delete/{!! $videos->id !!}">Eliminar</a>
                                <?php endif; ?>
                           </tr>
                         @endforeach
                       </tbody>
                       @else
                           <h2>No hay Videos registrados</h2>
                       @endif
                    </table>
                  </div>
                </div>
                </div>
              </div>
            </div>
          </div>
@endsection
