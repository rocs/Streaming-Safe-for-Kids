@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
              <?php
                $video = $datos[0];
                $categoria = $datos[1];
              ?>
                <div class="panel panel-default">
                    <div class="panel-heading">Video</div>
                    <div class="panel-body">
                      <form class="form-horizontal" role="form" method=""  action="">
                        <div class="form-group">
                        <div class="col-md-6">
                          <iframe width="724" height="380" src="{{$video->url}}" frameborder="0" allowfullscreen></iframe>
                        </div>
                        </div>
                      <div class="form-group">
                      <label for="restricted_mode" class="col-md-4 control-label">Categoria</label>
                      <div class="col-md-6">
                        <input readonly type="text" name="name" value="{{$categoria->name}}">
                      </div>
                      </div>
                      <div class="form-group">
                      <label for="restricted_mode" class="col-md-4 control-label">Descripcion</label>
                      <div class="col-md-6">
                        <input readonly type="text" name="description" value="{{$video->description}}">
                      </div>
                      </div>
                      <div class="form-group">
                      <label for="restricted_mode" class="col-md-4 control-label">Url</label>
                      <div class="col-md-6">
                        <input readonly type="text" name="url" value="{{$video->url}}">
                      </div>
                      </div>
                      <div class="form-group">
                          <div class="col-md-6 col-md-offset-4">
                            <a href="/video" class="btn btn-primary" >Regresar</a>
                          </div>
                      </div>
                      <div class="form-group">
                          <div class="col-md-6 col-md-offset-4">
                            <a href="/add/{{$video->id}}" class="btn btn-primary" >Añadir a Playlist</a>
                          </div>
                      </div>
                    </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
