@extends('layouts.app')
<script>
function myFunction() {
  alert("Cambien el /watch?v= por /embed/ en la url del video su url deveria quedar así: https://www.youtube.com/embed/qztch6_Uhuk no de este modo: https://www.youtube.com/watch?v=O5LmPPNEvYE");
}
</script>
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Videos</div>
                    <div class="panel-body">
                        <form class="form-horizontal" role="form" method="POST" action="/video/registro">
                            {{ csrf_field() }}
                            <div class="form-group{{ $errors->has('category_id') ? ' has-error' : '' }}">
                                <label for="category_id" class="col-md-4 control-label">Categoria</label>
                                <div class="col-md-6">
                                    <select name="category_id" id="category_id">
                                      <?php if (count($categories)>0): ?>
                                        <?php foreach ($categories as $category): ?>
                                          <option value="{{$category->id}}">{{$category->name}}</option>
                                        <?php endforeach; ?>
                                      <?php endif; ?>
                                    </select>
                                    @if ($errors->has('category_id'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('category_id') }}</strong>
                                    </span>
                                    @endif
                                    </div>
                            </div>
                            <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                                <label for="description" class="col-md-4 control-label">Descripcion</label>
                                <div class="col-md-6">
                                    <input id="description" type="text" class="form-control" name="description" value="{{ old('description') }}" required>
                                    @if ($errors->has('description'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('description') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('url') ? ' has-error' : '' }}">
                                <label for="mimimum_age" class="col-md-4 control-label">Url</label>
                                <div class="col-md-6">
                                    <input id="url" type="text" class="form-control" name="url" value="{{ old('url') }}" required>
                                    <a onclick="myFunction()">Tip para url correcta</a>
                                    @if ($errors->has('url'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('url') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">
                                        Crear
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
