@extends('layouts.app')
<script>
function myFunction() {
  alert("Cambien el /watch?v= por /embed/ en la url del video su url deveria quedar así: https://www.youtube.com/embed/qztch6_Uhuk no de este modo: https://www.youtube.com/watch?v=O5LmPPNEvYE");
}
</script>
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Editar video</div>
                    <div class="panel-body">
                      <?php
                        $video = $datos[0];
                        $categoria = $datos[1];
                        $categorias = $datos[2];
                       ?>
                        <form class="form-horizontal" role="form" method="POST"  action="{{ url('/video/update', $video->id) }}">
                            {{ csrf_field() }}
                              <input type="hidden" name="_method" value="PATCH">
                              <div class="form-group{{ $errors->has('category_id') ? ' has-error' : '' }}">
                                  <label for="category_id" class="col-md-4 control-label">Categoria</label>
                                  <div class="col-md-6">
                                      <select name="category_id" id="category_id">
                                          <option value="{{$categoria->id}}">{{$categoria->name}}</option>
                                          <?php foreach ($categorias as $category): ?>
                                            <option value="{{$category->id}}">{{$category->name}}</option>
                                          <?php endforeach; ?>
                                      </select>
                                      @if ($errors->has('category_id'))
                                          <span class="help-block">
                                          <strong>{{ $errors->first('category_id') }}</strong>
                                      </span>
                                      @endif
                                      </div>
                              </div>
                            <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                                <label for="description" class="col-md-4 control-label">Descricion</label>
                                <div class="col-md-6">
                                    <input id="description" type="text" class="form-control" name="description" value="{{ $video->description }}" required>
                                    @if ($errors->has('description'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('description') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('url') ? ' has-error' : '' }}">
                                <label for="mimimum_age" class="col-md-4 control-label">Url</label>
                                <div class="col-md-6">
                                    <input id="url" type="text" class="form-control" name="url" value="{{ $video->url }}" required>
                                    <a onclick="myFunction()">Tip para url correcta</a>
                                    @if ($errors->has('url'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('url') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">
                                        Editar
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
