@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
          <div class="panel panel-default">
                <div class="panel-body">
                  <a href="{{ url('list/register') }}"><button type="button" class="btn btn-success" name="button">Crear Playlist</button></a>
                  <br>
                  @if(count($playlists)>0)
                  <div class="table-responsive">
                    <table class = "table table-hover">
                       <caption>Playlists</caption>

                       <thead>
                          <tr>
                             <th>Descripcion</th>
                             <th>Privacidad</th>
                             <th>Acciones</th>
                          </tr>
                       </thead>
                       <tbody>
                         <?php //dd($playlists); ?>
                         @foreach ($playlists as $playlist)
                           <tr>

                              <td>{{$playlist->description}}</td>
                              <td>@if($playlist->public === 'Y')
                                    Publica
                                  @else
                                    Privada
                                  @endif</td>
                              <td><a class="btn btn-primary" href="list/show/{!! $playlist->id !!}">Ver</a>
                                <a class="btn btn-warning" href="list/edit/{!! $playlist->id !!}">Editar</a>
                                <?php //dd($playlist->id); ?>
                                <a class="btn btn-danger" href="list/delete/{!! $playlist->id !!}">Eliminar</a>
                           </tr>
                         @endforeach
                       </tbody>
                       @else
                           <h2>No hay Playlists registradas</h2>
                       @endif
                    </table>
                  </div>
                </div>
              </div>
              </div>
            </div>
          </div>
@endsection
