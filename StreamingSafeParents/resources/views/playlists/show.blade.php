@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                  <?php
                    $playlist = $datos[0];
                    $user = $datos[1];
                    $listItems = $datos[2];
                    $videos = $datos[3];
                   ?>
                    <div class="panel-heading"></div>
                    <div class="panel-body">
                      <form class="form-horizontal" role="form" method=""  action="">
                      <div class="form-group">
                      <label for="user" class="col-md-4 control-label">Creador</label>
                      <div class="col-md-6">
                        <input readonly type="user" name="name" value="{{$user->name}}">
                      </div>
                      </div>
                      <div class="form-group">
                      <label for="description" class="col-md-4 control-label">Descripcion</label>
                      <div class="col-md-6">
                        <input readonly type="text" name="description" value="{{$playlist->description}}">
                      </div>
                        </div>
                        <div class="form-group">
                      <label for="public" class="col-md-4 control-label">Buscador</label>
                      <div class="col-md-6">
                        @if($playlist->public === 'Y')
                        <input readonly type="text" name="public" value="Publica">
                        @else
                        <input readonly type="text" name="public" value="Privada">
                        @endif
                      </div>
                      </div>
                      <div class="form-group">
                      <label for="description" class="col-md-4 control-label">Ninos</label>
                      <div class="col-md-6">
                        <a href="/addK/{!! $playlist->id !!}" >Agregar</a>
                      </div>
                        </div>
                      <div class="form-group">
                          <div class="col-md-6 col-md-offset-4">
                            <a href="/list" class="btn btn-primary" >Regresar</a>
                          </div>
                      </div>
                    </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
              <div class="panel-body">
                    <div class="panel panel-default">
                      @if(count($listItems)>0)
                      <div class="table-responsive">
                        <table class = "table table-hover">
                          <!--<caption><a href="{{ url('list/register') }}"><button type="button" class="btn btn-success" name="button">Agregar Video</button></a></caption>-->
                           <br>
                           <thead>
                              <tr>
                                 <th>Description</th>
                                 <th>Video</th>
                                 <th>Acción</th>
                              </tr>
                           </thead>
                           <tbody>
                             <?php //dd($listItems, $videos); ?>
                            <?php foreach ($videos as $video): ?>
                             <?php foreach ($listItems as $listItem): ?>
                               <?php //dd($listItem); ?>
                               <?php if ($video->id == $listItem->video_id): ?>
                                 <tr>
                                    <td>{{$video->description}}</td>
                                    <td><iframe width="224" height="130" src="{{$video->url}}" frameborder="0" allowfullscreen></iframe></td>
                                    <td><a class="btn btn-danger" href="/item/delete/{!! $video->id !!}/{!! $playlist->id !!}">Eliminar</a></td>
                                 </tr>
                               <?php endif; ?>
                             <?php endforeach; ?>
                           <?php endforeach; ?>
                           </tbody>
                           @else
                               <h2>No hay Videos registrados</h2>
                           @endif
                        </table>
                      </div>
                    </div>
                    </div>
                  </div>
                </div>
              </div>
@endsection
