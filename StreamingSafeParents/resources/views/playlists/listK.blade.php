@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Lista de Ninos</div>
                <div class="panel-body">
                  <?php
                  $play = $datos[0];
                  $kids = $datos[1]; ?>
                    <form class="form-horizontal" role="form" method="POST" action="/Children/add">
                        {{ csrf_field() }}
                        <input type="hidden" name="playlist" value="{{$play->id}}">
                         @if(count($kids)>0)
                        <div class="form-group">
                            <label  class="col-md-4 control-label">Selecciona los ninos </label>
                            <?php $i = 0; ?>
                              @foreach ($kids as $kid)
                              <label class="col-md-4 control-label"></label>
                              <?php $i++ ?>
                              <div class="col-md-6">
                                <label><input type="checkbox" name="{{$i}}" value="{{$kid->id}}"> {{$kid->username}}</label>
                              </div>
                              @endforeach
                              <div class="form-group">
                                  <div class="col-md-6 col-md-offset-4">
                                      <button type="submit" class="btn btn-primary">
                                          Agregar
                                      </button>
                                  </div>
                              </div>
                        </div>
                        @else
                        <h1>No hay ninos disponibles para agregar </h1>
                        @endif
                        <a href="/list/show/{{$play->id}}">Regresar</a>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
