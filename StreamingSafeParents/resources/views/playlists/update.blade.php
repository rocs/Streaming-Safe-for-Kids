@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Registar</div>
                    <div class="panel-body">
                        <form class="form-horizontal" role="form" method="POST"  action="{{ url('/list/update', $playlist->id) }}">
                            {{ csrf_field() }}
                              <input type="hidden" name="_method" value="PATCH">

                            <div class="form-group{{ $errors->has('user_id') ? ' has-error' : '' }}">
                                <div class="col-md-6">
                                    <input id="user_id" type="hidden" class="form-control" name="user_id" value="{{ $playlist->user_id}}" required autofocus>
                                    @if ($errors->has('user_id'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('user_id') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                                <label for="description" class="col-md-4 control-label">Descripcion</label>

                                <div class="col-md-6">
                                    <input id="description" type="text" class="form-control" name="description" value="{{ $playlist->description }}" required>

                                    @if ($errors->has('description'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('description') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('public') ? ' has-error' : '' }}">
                                <label for="public" class="col-md-4 control-label">Privacidad</label>

                                <div class="col-md-6">
                                  <select name="public">
                                    @if($playlist->public == 'Y')
                                    <option value="Y">Publica</option>
                                    <option value="N">Privada</option>
                                    @else
                                    <option value="N">Privada</option>
                                    <option value="Y">Publica</option>
                                    @endif
                                  </select>

                                    @if ($errors->has('public'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('public') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">
                                        Editar
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
