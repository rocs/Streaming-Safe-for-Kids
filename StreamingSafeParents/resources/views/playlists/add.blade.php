@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Añadir a Playlist</div>
                    <div class="panel-body">
                      <?php
                      $video = $datos[0];
                      $playlists = $datos[1];
                       ?>
                        <form class="form-horizontal" role="form" method="POST" action="/add">
                            {{ csrf_field() }}

                            <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                                <label for="des" class="col-md-4 control-label">Video</label>

                                <div class="col-md-6">
                                    <input readonly id="des" type="text" class="form-control" name="des" value="{{ $video->description }}" required autofocus>
                                    <input  id="video_id" type="hidden" class="form-control" name="video_id" value="{{ $video->id }}" required autofocus>

                                    @if ($errors->has('video_id'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('video_id') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('playlist_id') ? ' has-error' : '' }}">
                                <label for="playlist_id" class="col-md-4 control-label">Playlist</label>
                                <div class="col-md-6">
                                  <select name="playlist_id">
                                    <?php if (count($playlists)>0): ?>
                                      <?php foreach ($playlists as $playlist): ?>
                                        <option value="{{$playlist->id}}">{{$playlist->description}}</option>
                                      <?php endforeach; ?>
                                    <?php else: ?>
                                      <h5>No hay playlist</h5>
                                    <?php endif; ?>
                                  </select>

                                    @if ($errors->has('playlist_id'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('playlist_id') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">
                                        Registar
                                    </button>
                                    <a href="/video/show/{{ $video->id }}">Atras</a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
