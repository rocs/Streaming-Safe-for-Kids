@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Registar</div>
                    <div class="panel-body">
                        <form class="form-horizontal" role="form" method="POST" action="/list/registro">
                            {{ csrf_field() }}

                            <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                                <label for="description" class="col-md-4 control-label">Descripcion</label>

                                <div class="col-md-6">
                                    <input id="description" type="text" class="form-control" name="description" value="{{ old('description') }}" required autofocus>

                                    @if ($errors->has('description'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('description') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('public') ? ' has-error' : '' }}">
                                <label for="public" class="col-md-4 control-label">Privacidad</label>
                                <div class="col-md-6">
                                  <select name="public">
                                    <option value="Y">Publica</option>
                                    <option value="N">Privada</option>
                                  </select>

                                    @if ($errors->has('public'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('public') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">
                                        Registar
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
