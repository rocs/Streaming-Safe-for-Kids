@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
          <div class="panel panel-default">
                <div class="panel-body">
                  <br>
                  @if(count($users)>0)
                  <div class="table-responsive">
                    <table class = "table table-hover">
                       <caption>Usuarios</caption>
                       <thead>
                          <tr>
                             <th>Nombre</th>
                             <th>Rol</th>
                            <th>Cambio</th>
                          </tr>
                       </thead>
                       <tbody>
                         @foreach ($users as $user)
                           <tr>
                              <td>{{$user->name}}</td>
                              <td>@if ($user->rol === 'C')
                                Contribuidor
                                @else
                                Padre
                              @endif</td>
                              <td> <a class="" href="cambiar/{!! $user->id !!}/{!! $user->rol !!}">Cambiar</a></td>
                           </tr>
                         @endforeach
                       </tbody>
                       @else
                           <h2>No hay Etiquetas registradas</h2>
                       @endif
                    </table>
                  </div>
                </div>
                </div>
              </div>
            </div>
          </div>
@endsection
