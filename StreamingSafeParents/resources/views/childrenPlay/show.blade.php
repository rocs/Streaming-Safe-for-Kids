@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                  <?php
                    $datos=$cosas[0];
                    $niño=$cosas[1];
                   ?>
                    <div class="panel-heading">Lista de Playlist del niño</div>
                    <div class="panel-body">
                      <form class="form-horizontal" role="form" method=""  action="">
                      <div class="form-group">
                        <?php foreach ($datos as $dato): ?>
                          <label for="restricted_mode" class="col-md-4 control-label"></label>
                          <div class="col-md-6">
                            <input readonly type="text" name="description" value="{{$dato->description}}">
                            <a class="glyphicon glyphicon-remove-sign" href="/Children/destroy/{{$niño}}/{{$dato->id}}"></a>
                          </div>
                        <?php endforeach; ?>
                      </div>
                      <div class="form-group">
                          <div class="col-md-6 col-md-offset-4">
                            <a href="/ninos" class="btn btn-primary" >Regresar</a>
                          </div>
                      </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
