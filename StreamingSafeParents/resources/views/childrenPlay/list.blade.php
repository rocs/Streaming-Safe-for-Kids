@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Niños</div>
                <div class="panel-body">
                  <?php
                  $kid = $datos[0];
                  $plays = $datos[1]; ?>
                    <form class="form-horizontal" role="form" method="POST" action="/Children/agregar">
                        {{ csrf_field() }}
                        <input type="hidden" name="id" value="{{$kid->id}}">
                        <div class="form-group">
                            <label  class="col-md-4 control-label">Asignar Playlist a {{$kid->name}}  </label>
                            <select class="" name="playlist">
                              <label class="col-md-4 control-label"></label>
                              <div class="col-md-6">
                                @foreach ($plays as $play)
                                 <option value="{{$play->id}}">{{$play->description}}</option>
                                @endforeach
                              </div>
                            </select>
                        </div>
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Agregar
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
