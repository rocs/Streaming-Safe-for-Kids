@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">{{$kid->username}}</div>
                    <div class="panel-body">
                      <form class="form-horizontal" role="form" method=""  action="">
                        <div class="form-group">
                        <label for="username" class="col-md-4 control-label">Nombre de Usuario</label>
                        <div class="col-md-6">
                          <input readonly type="text" name="name" value="{{$kid->username}}">
                        </div>
                          </div>
                      <div class="form-group">
                      <label for="restricted_mode" class="col-md-4 control-label">Nombre de usuario</label>
                      <div class="col-md-6">
                        <input type="readonly" name="name" value="{{$kid->username}}">
                      </div>
                      </div>
                    <div class="panel-body">
                      <form class="form-horizontal" role="form" method=""  action="">
                      <div class="form-group">
                      <label for="restricted_mode" class="col-md-4 control-label">Contraseña</label>
                      <div class="col-md-6">
                        <input readonly type="password" name="name" value="{{$kid->password}}">
                      </div>
                      </div>
                      <div class="form-group">
                      <label for="restricted_mode" class="col-md-4 control-label">Cumpleaños</label>
                      <div class="col-md-6">
                        <input readonly type="text" name="name" value="{{$kid->birthdate}}">
                      </div>
                        </div>
                        <div class="form-group">
                      <label for="restricted_mode" class="col-md-4 control-label">Buscador</label>
                      <div class="col-md-6">
                        @if($kid->search === 'E')
                        <input readonly type="text" name="name" value="Habilitado">
                        @else
                        <input readonly type="text" name="name" value="Desabilitado">
                        @endif
                      </div>
                      </div>
                      <div class="form-group">
                        <label for="restricted_mode" class="col-md-4 control-label">Modo retrictivo</label>
                        <div class="col-md-6">
                          @if($kid->restricted_mode === 'Y')
                          <input readonly type="text" name="name" value="Activado">
                          @else
                          <input readonly type="text" name="name" value="Desactivado">
                          @endif
                        </div>
                      </div>
                      <div class="form-group">
                          <div class="col-md-6 col-md-offset-4">
                            <a href="/ninos" class="btn btn-primary" >Regresar</a>
                          </div>
                      </div>
                    </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
