@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
          <div class="panel panel-default">
                <div class="panel-body">
                  <a href="{{ url('ninos/register') }}"><button type="button" class="btn btn-success" name="button">Registrar Niño</button></a>
                  <br>
                  <?php //dd($kids); ?>
                  @if(count($kids)>0)
                  <div class="table-responsive">
                    <table class = "table table-hover">
                       <caption>Niños</caption>

                       <thead>
                          <tr>
                             <th>Nombre de usuario</th>
                             <th>Buscador</th>
                             <th>Modo restrictivo</th>
                             <th>Playlist</th>
                             <th>Acciones</th>
                          </tr>
                       </thead>
                       <tbody>
                         @foreach ($kids as $kid)
                           <tr>
                              <td>{{$kid->username}}</td>
                              <td>@if($kid->search === 'E')
                                    Habilitado
                                  @else
                                    Desabilitado
                                  @endif</td>
                              <td>@if($kid->restricted_mode === 'Y')
                                    Activado
                                    @else
                                    Desactivado
                                    @endif</td>
                                    <td><a class="btn btn-primary" href="Children/show/{!! $kid->id !!}">Ver asignaturas</a>
                                    <a class="btn btn-primary" href="Children/register/{!! $kid->id !!}">Asignar</a> </td>
                              <td><a class="btn btn-primary" href="ninos/show/{!! $kid->id !!}">Ver</a>
                                <a class="btn btn-warning" href="ninos/edit/{!! $kid->id !!}">Editar</a>
                              <a class="btn btn-danger" href="ninos/destroy/{!! $kid->id !!}">Eliminar</a></td>
                           </tr>
                         @endforeach
                       </tbody>
                       @else
                           <h2>No hay Niños registrados</h2>
                       @endif
                    </table>
                  </div>
                </div>
                </div>
              </div>
            </div>
          </div>
@endsection
