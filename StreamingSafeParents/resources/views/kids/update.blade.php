@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Registar</div>
                    <div class="panel-body">
                        <form class="form-horizontal" role="form" method="POST"  action="{{ url('/ninos/update', $kid->id) }}">
                            {{ csrf_field() }}
                              <input type="hidden" name="_method" value="PATCH">

                            <div class="form-group{{ $errors->has('username') ? ' has-error' : '' }}">
                                <label for="username" class="col-md-4 control-label">Nombre de Usuario</label>

                                <div class="col-md-6">
                                    <input id="username" type="text" class="form-control" name="username" value="{{ $kid->username}}" required autofocus>

                                    @if ($errors->has('username'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('username') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                <label for="password" class="col-md-4 control-label">Contraseña</label>

                                <div class="col-md-6">
                                    <input id="password" type="password" class="form-control" name="password" value="{{ $kid->password }}" required>

                                    @if ($errors->has('password'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('birthday') ? ' has-error' : '' }}">
                                <label for="birthday" class="col-md-4 control-label">Cumpleaños</label>

                                <div class="col-md-6">
                                    <input id="birthday" type="date" class="form-control" name="birthday" value='{{$kid->birthdate}}' required>

                                    @if ($errors->has('birthday'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('birthday') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('search') ? ' has-error' : '' }}">
                                <label for="search" class="col-md-4 control-label">Buscador</label>

                                <div class="col-md-6">
                                  <select name="search">
                                    @if($kid->search === 'E')
                                    <option value="E">Habilitar</option>
                                    <option value="D">Desabilitar</option>
                                    @else
                                    <option value="D">Desabilitar</option>
                                    <option value="E">Habilitar</option>
                                    @endif
                                  </select>

                                    @if ($errors->has('search'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('search') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('restricted_mode') ? ' has-error' : '' }}">
                                <label for="restricted_mode" class="col-md-4 control-label">Modo retrictivo</label>

                                <div class="col-md-6">
                                  <select name="restricted_mode">
                                    @if($kid->restricted_mode == 'Y')
                                    <option value="Y">Activar</option>
                                    <option value="N">Desactivar</option>
                                    @else
                                    <option value="N">Desactivar</option>
                                    <option value="Y">Activar</option>
                                    @endif
                                  </select>

                                    @if ($errors->has('restricted_mode'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('restricted_mode') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">
                                        Editar
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
