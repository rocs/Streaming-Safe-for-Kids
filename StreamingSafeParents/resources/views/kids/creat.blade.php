@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Registar</div>
                    <div class="panel-body">
                        <form class="form-horizontal" role="form" method="POST" action="/registro">
                            {{ csrf_field() }}

                            <div class="form-group{{ $errors->has('username') ? ' has-error' : '' }}">
                                <label for="username" class="col-md-4 control-label">Nombre de Usuario</label>

                                <div class="col-md-6">
                                    <input id="username" type="text" class="form-control" name="username" value="{{ old('username') }}" required autofocus>

                                    @if ($errors->has('username'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('username') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                <label for="password" class="col-md-4 control-label">Contraseña</label>

                                <div class="col-md-6">
                                    <input id="password" type="password" class="form-control" name="password" value="{{ old('password') }}" required>

                                    @if ($errors->has('password'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('birthday') ? ' has-error' : '' }}">
                                <label for="birthday" class="col-md-4 control-label">Cumpleaños</label>

                                <div class="col-md-6">
                                    <input id="birthday" type="date" class="form-control" name="birthday" required>

                                    @if ($errors->has('birthday'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('birthday') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('search') ? ' has-error' : '' }}">
                                <label for="search" class="col-md-4 control-label">Buscador</label>

                                <div class="col-md-6">
                                  <select name="search">
                                    <option value="E">Habilitar</option>
                                    <option value="D">Desabilitar</option>
                                  </select>

                                    @if ($errors->has('search'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('search') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('restricted_mode') ? ' has-error' : '' }}">
                                <label for="restricted_mode" class="col-md-4 control-label">Modo retrictivo</label>

                                <div class="col-md-6">
                                  <select name="restricted_mode">
                                    <option value="Y">Activar</option>
                                    <option value="N">Desactivar</option>
                                  </select>

                                    @if ($errors->has('restricted_mode'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('restricted_mode') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">
                                        Registar
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
