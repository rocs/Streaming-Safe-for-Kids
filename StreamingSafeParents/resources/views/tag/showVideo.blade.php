@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                  <?php
                    $datos = $cosas[0];
                    $video = $cosas[1];
                   ?>
                    <div class="panel-heading">Etiquetas</div>
                    <div class="panel-body">
                      <form class="form-horizontal" role="form" method=""  action="">
                      <div class="form-group">
                        <?php foreach ($datos as $dato): ?>
                          <label for="restricted_mode" class="col-md-4 control-label"></label>
                          <div class="col-md-6">
                            <input readonly type="text" name="description" value="{{$dato->description}}">
                            <?php if (Auth::user()->rol == "C"): ?>
                              <a class="glyphicon glyphicon-remove-sign" href="/taglist/destroy/{{$video}}/{{$dato->id}}"></a>
                            <?php endif; ?>
                          </div>
                        <?php endforeach; ?>
                      </div>
                      <div class="form-group">
                          <div class="col-md-6 col-md-offset-4">
                            <a href="/video" class="btn btn-primary" >Regresar</a>
                            <?php if (Auth::user()->rol == "C"): ?>
                              <a href="/taglist/regist/{{$video}}" class="btn btn-primary" >Agregar Tag</a>
                            <?php endif; ?>
                          </div>
                      </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
