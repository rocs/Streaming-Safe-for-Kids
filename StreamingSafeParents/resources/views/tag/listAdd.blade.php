@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Lista de Tag</div>
                <div class="panel-body">
                  <?php
                  $tags = $datos[0];
                  $id = $datos[1]; ?>
                    <form class="form-horizontal" role="form" method="POST" action="/taglist/agregar">
                        {{ csrf_field() }}
                        <input type="hidden" name="id" value="{{$id}}">
                        <div class="form-group">
                            <label  class="col-md-4 control-label">Selecciona las etiquetas </label>
                            <?php $i = 0; ?>
                              @foreach ($tags as $tag)
                              <label class="col-md-4 control-label"></label>
                              <?php $i++ ?>
                              <div class="col-md-6">
                                <label><input type="checkbox" name="{{$i}}" value="{{$tag->id}}"> {{$tag->description}}</label>
                              </div>
                              @endforeach
                        </div>
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Agregar
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
