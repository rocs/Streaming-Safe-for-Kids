@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
          <div class="panel panel-default">
                <div class="panel-body">
                  <a href="{{ url('tag/register') }}"><button type="button" class="btn btn-success" name="button">Crear Etiqueta</button></a>
                  <br>
                  @if(count($tags)>0)
                  <div class="table-responsive">
                    <table class = "table table-hover">
                       <caption>Etiquetas</caption>
                       <thead>
                          <tr>
                             <th>Descripcion</th>
                             <th>Accion</th>
                          </tr>
                       </thead>
                       <tbody>
                         @foreach ($tags as $tag)
                           <tr>
                              <td>{{$tag->description}}</td>
                              <td><a class="btn btn-primary" href="tag/show/{!! $tag->id !!}">Ver</a>
                                <a class="btn btn-warning" href="tag/edit/{!! $tag->id !!}">Editar</a>
                                  <a class="btn btn-danger" href="tag/destroy/{!! $tag->id !!}">Eliminar</a></td>
                           </tr>
                         @endforeach
                       </tbody>
                       @else
                           <h2>No hay Etiquetas registradas</h2>
                       @endif
                    </table>
                  </div>
                </div>
              </div>
              </div>
            </div>
          </div>
@endsection
