@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">{{$category->name}}</div>
                    <div class="panel-body">
                      <form class="form-horizontal" role="form" method=""  action="">
                      <div class="form-group">
                      <label for="restricted_mode" class="col-md-4 control-label">Nombre</label>
                      <div class="col-md-6">
                        <input readonly type="text" name="name" value="{{$category->name}}">
                      </div>
                      </div>
                      <form class="form-horizontal" role="form" method=""  action="">
                      <div class="form-group">
                      <label for="restricted_mode" class="col-md-4 control-label">Descripcion</label>
                      <div class="col-md-6">
                        <input readonly type="text" name="description" value="{{$category->description}}">
                      </div>
                      </div>
                      <div class="form-group">
                      <label for="restricted_mode" class="col-md-4 control-label">Edad Minima</label>
                      <div class="col-md-6">
                        <input readonly type="text" name="mimimum_age" value="{{$category->mimimum_age}}">
                      </div>
                      </div>
                      <div class="form-group">
                          <div class="col-md-6 col-md-offset-4">
                            <a href="/categories" class="btn btn-primary" >Regresar</a>
                          </div>
                      </div>
                    </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
