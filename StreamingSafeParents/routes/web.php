<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index');

//Kids
Route::get('/ninos', 'KidsController@index');
Route::get('/ninos/register', 'KidsController@create');
Route::post('/registro', 'KidsController@store');
Route::get('ninos/destroy/{id}', 'KidsController@destroy');
Route::get('ninos/edit/{id}', 'KidsController@edit');
Route::match(['put', 'patch'], 'ninos/update/{id}', 'KidsController@update');
Route::get('ninos/show/{id}', 'KidsController@show');

//Tags
Route::get('/tag', 'TagController@index');
Route::get('tag/register', 'TagController@create');
Route::post('tag/registro', 'TagController@store');
Route::get('tag/show/{id}', 'TagController@show');
Route::get('tag/edit/{id}', 'TagController@edit');
Route::match(['put', 'patch'], 'tag/update/{id}', 'TagController@update');
Route::get('tag/destroy/{id}', 'TagController@destroy');

//Tags Video
Route::get('taglist/register/{id}', 'TagVideoController@index');
Route::get('taglist/regist/{id}', 'TagVideoController@video');
Route::post('taglist/agregar', 'TagVideoController@create');
Route::get('taglist/show/{id}','TagVideoController@show');
Route::get('taglist/destroy/{id}/{idtag}', 'TagVideoController@destroy');


//Categories
Route::get('/categories', 'CategoryController@index');
Route::get('category/register', 'CategoryController@create');
Route::post('category/registro', 'CategoryController@store');
Route::get('category/show/{id}', 'CategoryController@show');
Route::get('category/edit/{id}', 'CategoryController@edit');
Route::match(['put', 'patch'], 'category/update/{id}', 'CategoryController@update');
Route::get('category/destroy/{id}', 'CategoryController@destroy');

//Videos
Route::get('/video', 'VideoController@index');
Route::get('video/register', 'VideoController@create');
Route::post('video/registro', 'VideoController@store');
Route::get('video/edit/{id}', 'VideoController@edit');
Route::match(['put', 'patch'], 'video/update/{id}', 'VideoController@update');
Route::get('video/destroy/{id}', 'VideoController@destroy');
Route::get('video/delete/{id}', 'VideoController@destroy');
Route::get('video/show/{id}', 'VideoController@show');

//Playlist
Route::get('/list', 'PlaylistController@index');
Route::get('list/register', 'PlaylistController@create');
Route::post('list/registro', 'PlaylistController@store');
Route::get('list/edit/{id}', 'PlaylistController@edit');
Route::match(['put', 'patch'], 'list/update/{id}', 'PlaylistController@update');
Route::get('list/destroy/{id}', 'PlaylistController@destroy');
Route::get('list/delete/{id}', 'PlaylistController@destroy');
Route::get('list/show/{id}', 'PlaylistController@show');

Route::get('add/{id}', 'PlaylistController@add');//show the view wher you store a video in a playlist
Route::post('/add', 'PlaylistController@play');//store a video in a playlist
Route::get('item/delete/{id_video}/{id_play}', 'PlaylistController@delete');

Route::get('Children/register/{id}', 'ChildrenPlayListController@index');
Route::post('/Children/agregar','ChildrenPlayListController@create');
Route::post('/Children/add','ChildrenPlayListController@store');
Route::get('Children/show/{id}','ChildrenPlayListController@show');
Route::get('/addK/{id}','ChildrenPlayListController@kid');
Route::get('Children/destroy/{id}/{playlistid}', 'ChildrenPlayListController@destroy');



Route::get('/padres','PadreController@index');
Route::get('/cambiar/{id}/{rol}','PadreController@cambio');
