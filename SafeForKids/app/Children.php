<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Children extends Authenticatable
{

  protected $table = "childrens";
  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
          'name', 'user_id', 'password','birthday','search','restricted_mode',
  ];

}
