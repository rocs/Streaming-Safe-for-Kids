<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Playlist extends Model
{

  protected $table = "playlists";

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
      'description', 'public',
  ];

}
