<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Playlist;
use App\ListItems;
use App\Video;
use App\Category;
use Auth;
class PlaylistController extends Controller
{
  public function __construct()
{
    $this->middleware('auth');
}
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        $hoy=getdate();
        $nacimiento = strtotime(Auth::user()->birthdate);
        $nacimiento = date('Y',$nacimiento);
        $edad = $hoy['year']-$nacimiento;
        $playlist = Playlist::find($id);
        $items = ListItems::where('playlist_id','=',$id)->get();
        $videos = array();
        foreach ($items as $item) {
          $val = Video::find($item->video_id);
          $cate = Category::find($val->category_id);
          if ($edad >= $cate->mimimum_age) {
            $videos[]= Video::find($item->video_id);
          }
        }
        //dd($videos);
        $datos = array($playlist,$videos);
        return view('media', compact('datos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
