<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Video;
use App\Category;
use App\Tag;
use App\TagVideo;
use App\Playlist;
use App\ChildrenPlaylist;
use Auth;

class VideoController extends Controller
{
  public function __construct()
{
    $this->middleware('auth');
}
/**
 * Display a listing of the resource.
 *
 * @return \Illuminate\Http\Response
 */
public function index()
{
  $video = Video::all();
  $category = Category::all();
  //$playlist = Playlist::find($playId);
  $cosas = array( $video, $category);
  //dd($video);
  return view('home', compact('cosas'));
}

/**
 * Show the form for creating a new resource.
 *
 * @return \Illuminate\Http\Response
 */
public function create()
{
   //
}
/**
 * Store a newly created resource in storage.
 *
 * @param  \Illuminate\Http\Request  $request
 * @return \Illuminate\Http\Response
 */
public function store(Request $request)
{
  //
}
/**
 * Display the specified resource.
 *
 * @param  int  $id
 * @return \Illuminate\Http\Response
 */
public function show($id)
{
  $video = Video::find($id);
  $categoria = Category::find($video->category_id);
  $datos = array($video, $categoria);
  return view('videos/show', compact('datos'));
}

/**
 * Display the specified resource.
 *
 * @param  int  $id
 * @return \Illuminate\Http\Response
 */
public function search(Request $request)
{
  $childrenplaylists = ChildrenPlaylist::where('children_id', '=', Auth::id())->get();
  $playlist = array();
  foreach ($childrenplaylists as $childrenplaylist) {
        $playlist[] = Playlist::find($childrenplaylist->playlist_id)->first();
  }
  $category = Category::where('name','=',$request->input("q"))->get();
  $categorias = Category::all();
  $tag = Tag::where('description','like',$request->input("q"))->get();
  $videosName = Video::where('description','LIKE','%'.$request->input("q").'%')->get();
  //dd($videosName);
  //dd($tag);
  if (count($tag) > 0) {
    $tag = $tag[0];
    $tagVideos = TagVideo::where('tag_id','=',$tag->id)->get();
    //dd($tagVideos);
  }else {
  $tagVideos = TagVideo::where('tag_id','=','q')->get();
  }
  if (count($category) > 0) {
    $category = $category[0];
    $videoCat = Video::where('category_id','=',$category->id)->get();
  }else {
    $videoCat = Video::where('category_id','=','q')->get();
  }
  $videotags = array();
  foreach ($tagVideos as $tagVideo) {
    $videotag = Video::find($tagVideo->video_id);
    $videotags[] = ($videotag);
  }

  $datos = array($playlist);
  if (count($videoCat) > 0 && count($videotags) > 0 && count($videosName) > 0) {
    $datos = array($playlist,$categorias,$videoCat, $videotags, $videosName);
  }elseif (count($videoCat) > 0 && count($videotags) > 0) {
    $datos = array($playlist,$categorias,$videoCat, $videotags);
  }elseif (count($videoCat) > 0 && count($videosName) > 0) {
    $datos = array($playlist,$categorias,$videoCat, $videosName);
  }elseif (count($videosName) > 0 && count($videotags) > 0) {
    $datos = array($playlist,$categorias,$videosName, $videotags);
  } elseif (count($videoCat) > 0) {
    $datos = array($playlist,$categorias,$videoCat);
  } elseif (count($videotags) > 0) {
    $datos = array($playlist,$categorias,$videotags);
  }elseif (count($videosName) > 0) {
    $datos = array($playlist,$categorias,$videosName);
  }
//dd($playlist);
  return view('home', compact('datos'));
}

/**
 * Show the form for editing the specified resource.
 *
 * @param  int  $id
 * @return \Illuminate\Http\Response
 */
public function edit($id)
{
  //
}
/**
 * Update the specified resource in storage.
 *
 * @param  \Illuminate\Http\Request  $request
 * @param  int  $id
 * @return \Illuminate\Http\Response
 */
public function update(Request $request, $id)
{
  //

}
/**
 * Remove the specified resource from storage.
 *
 * @param  int  $id
 * @return \Illuminate\Http\Response
 */
public function destroy($id)
{
  //
}
}
