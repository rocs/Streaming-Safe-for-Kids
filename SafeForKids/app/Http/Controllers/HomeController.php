<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Video;
use App\Category;
use App\Playlist;
use App\ChildrenPlaylist;
use Auth;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**3
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $video = Video::all();
      $category = Category::all();
      $childrenplaylists = ChildrenPlaylist::where('children_id', '=', Auth::id())->get();
      $playlist = array();
      foreach ($childrenplaylists as $childrenplaylist) {
            $playlist[] = Playlist::where('id','=', $childrenplaylist->playlist_id)->first();
      }
      $cosas = array( $playlist, $video, $category);
      //dd($video);
      return view('home', compact('cosas'));
    }
}
