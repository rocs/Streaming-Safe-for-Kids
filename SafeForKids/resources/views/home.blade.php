@extends('layouts.app')

@section('main-content')
<div class="container">
    <div class="section">
        <div class="row centered">
            <div class="col s12">
                <div class="section centered">
                    <div class="col s12">
                        <span class="title">Recently Added</span>
                    </div>
                    <?php if (count($cosas)>0): ?>
                      <?php $videos = $cosas[1];
                      $categorias = $cosas[2];?>
                      <?php foreach ($videos as $video): ?>
                        <?php //$url = explode("embed/", $video->url)[1]; dd($url);?>
                        <div class="col m6">
                          <div class="card">
                            <div class="card-image waves-effect waves-block waves-light">
                              <iframe class="embed-responsive-item" width="635" height="400" src="{{$video->url}}" frameborder="0" allowfullscreen></iframe>
                            </div>
                          <div class="card-content">
                            <p class="area"><a href="#">{{$video->description}}</a></p>
                            <?php foreach ($categorias as $categoria): ?>
                              <?php if ($video->category_id == $categoria->id): ?>
                                <a href="#" data-deeplink="adbeus://supercoffee"><span class="card-title activator brown-text text-darken-4">{{$categoria->name}}</span></a>
                              <?php endif; ?>
                            <?php endforeach; ?>
                          </div>
                        </div>
                      </div>
                      <?php endforeach; ?>
                    <?php else: ?>

                    <?php endif; ?>


              </div>
            </div>
        </div>
    </div>
</div>

@endsection
