<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Striaming Safe For Kids</title>

        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

        <!--Import Google Icon Font-->
      <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
      <!--Import materialize.css-->
      <link type="text/css" rel="stylesheet" href="css/materialize.min.css"  media="screen,projection"/>

      <!--Let browser know website is optimized for mobile-->
      <meta name="viewport" content="width=device-width, initial-scale=1.0"/>

      <!-- Compiled and minified CSS -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.8/css/materialize.min.css">

  <!-- Compiled and minified JavaScript -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.8/js/materialize.min.js"></script>

  <!--Import css.css-->
  <link type="text/css" rel="stylesheet" href="css/css.css"  media="screen,projection"/>

    </head>
    <body>
      <video autoplay loop muted id="background">
        <source src="videos/video.mp4" type="video/mp4">
      </video>
      <div id="headerwrap">
          <div class="container">
              <div class="row centered">
                <div class="col-md-12">
                  <h1><strong>Bienvenidos a Streaming</strong></h1>
                  <h1><strong>Safe for Kids</strong></h1>
                </div>
              </div>
          </div> <!--/ .container -->
      </div><!--/ #headerwrap -->

      <div id="headerwrap" style="opacity: 0.7; ">
          <div class="container">
              <div class="row centered">
                <!--<div class="col-md-4">
                <img class="img-responsive" height="442" width="400" src="img/welcome/He.jpg" alt="" />
              </div>-->
              <div class="col-md-12">
                  <h2>Puedes empezar a ver videos</h2>

                  <div class="login-box">


                  @if (count($errors) > 0)
                      <div class="alert alert-danger">
                          <strong>Whoops!</strong> someproblems<br><br>
                          <ul>
                              @foreach ($errors->all() as $error)
                                  <li>{{ $error }}</li>
                              @endforeach
                          </ul>
                      </div>
                  @endif

                  <div class="login-box-body col-md-12">
                    <div class="login-logo">
                        <h3><b>Login</h3>
                    </div><!-- /.login-logo -->
                  <p class="login-box-msg">  </p>
                  <form action="{{ url('/login') }}" method="post">
                      <input type="hidden" name="_token" value="{{ csrf_token() }}">
                      <div class="form-group has-feedback">
                          <input type="text" class="form-controls" placeholder="Nombre de usuario" name="username"/>
                          <span class="glyphicon glyphicon-pencil form-control-feedback"></span>
                      </div>
                      <div class="form-group has-feedback">
                          <input type="password" class="form-controls" placeholder="Contraseña" name="password"/>
                          <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                      </div>
                      <div class="form-group has-feedback col-xs-12">

                              <button type="submit" class="btn btn-primary btn-block btn-flat">Sing in</button>

                      </div>
                  </form>
              </div><!-- /.login-box-body -->

              </div><!-- /.login-box -->

            </div>
                </div>
          </div> <!--/ .container -->
      </div><!--/ #introwrap -->

        <!--Import jQuery before materialize.js-->
        <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
        <script type="text/javascript" src="js/materialize.min.js"></script>
    </body>
</html>
