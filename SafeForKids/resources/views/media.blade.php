@extends('layouts.app1')

@section('main-content')

</script>

	<?php
	$playlist = $datos[0];
	$videos = $datos[1];?>
	<div class="container-fluid spark-screen">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">

				<div class="panel panel-default">
					<div class="panel-heading"><label id="info"></label></div>
					<div class="panel-body">
						<div class="embed-responsive embed-responsive-16by9">
						  <div id="player"></div>
						</div>

						<label type="text" id="url" name="url"></label>
					</div>
					<script>


		// 3. This function creates an <iframe> (and YouTube player)
		//    after the API code downloads.
		var videos = [];
	 <?php foreach ($videos as $video): ?>
	 var url = "{{$video->url}}";
		videos.push (url.split("embed/")[1]);
		<?php endforeach; ?>

		var i = 1;
		var player;
		function onYouTubeIframeAPIReady() {
			player = new YT.Player('player', {
				height: '360',
				width: '640',
				class: "embed-responsive-item",
				videoId: videos[0],
				playerVars: {rel: 0, showinfo: 0, iv_load_policy: 3},
				events: {
					'onReady': onPlayerReady,
					'onStateChange': onPlayerStateChange
				}
			});

		}

		// 4. The API will call this function when the video player is ready.
		function onPlayerReady(event) {
			event.target.playVideo();
		}
		// 5. The API calls this function when the player's state changes.
		//    The function indicates that when playing a video (state=1),
		//    the player should play for six seconds and then stop.
		var done = false;
		function onPlayerStateChange(event) {

			if (event.data == YT.PlayerState.ENDED && !done) {
				player.loadVideoById ({'videoId' : videos[i]} );
				i++;
				if (videos.length == i) {
					done = true;
				}

			}
		}
		function stopVideo() {
			player.stopVideo();
		}
	</script>
				</div>
			</div>
		</div>
	</div>
@endsection
