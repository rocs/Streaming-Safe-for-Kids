<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

        <!-- Sidebar user panel (optional) -->
        @if (! Auth::guest())
            <div class="user-panel">
                <div class="pull-left image">
                    <img src="/img/icons/smileblue.png" class="img-circle" alt="User Image" />
                </div>
                <div class="pull-left info">
                    <p>{{ Auth::user()->username }}</p>

                </div>
            </div>
        @endif

        <!-- search form (Optional) -->
        <?php if (Auth::user()->search == "E"): ?>
          <form action="/video/search/" method="post" class="sidebar-form">
            {{ csrf_field() }}
                  <div class="input-group">
                  <input type="text" name="q" id="q" class="form-control" placeholder="Categorias/Etiquetas"/>
                <span class="input-group-btn">
                  <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i></button>
                </span>
              </div>

          </form>
        <?php endif; ?>

        <!-- /.search form -->

        <!-- Sidebar Menu -->
        <ul class="sidebar-menu">
            <li class="header"></li>
            <!-- Optionally, you can add icons to the links -->
            <li class="treeview">
              <?php $x = strpos($_SERVER["REQUEST_URI"], 'videos');
              if ($x == 1) {
              	$x = true;
              } else {
              	$x=false;
              }
              ?>
              <?php if ($_SERVER["REQUEST_URI"] == '/home' || $x === true || $_SERVER["REQUEST_URI"] == '/video/search/'): ?>
                <?php if ($x === true): ?>
                  <a href="/media/{{$playlist->id}}"><i class='fa fa-link'></i> <span> {{$playlist->description}} </span> <i class="fa fa-angle-left pull-right"></i></a>
                <?php else: ?>
                  <?php foreach ($playlist as $p): ?>
                      <a href="/media/{{$p->id}}"><i class='fa fa-link'></i> <span>{{$p->description}} </span> <i class=" pull-right"></i></a>
                  <?php endforeach; ?>
                <?php endif; ?>

              <?php else: ?>
                <a href="#"><i class='fa fa-link'></i> <span> {{$playlist->description}} </span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                  <?php foreach ($videos as $video): ?>
                    <li><a href="/videos/{{$playlist->id}}/{{$video->id}}">{{$video->description}}</a></li>
                  <?php endforeach; ?>
                </ul>
              <?php endif; ?>
            </li>
        </ul><!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
</aside>
