<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Compiled and minified CSS -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.8/css/materialize.min.css">

<!-- Compiled and minified JavaScript -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.8/js/materialize.min.js"></script>

 <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

<!--Import css.css-->
<link type="text/css" rel="stylesheet" href="css/css.css"  media="screen,projection"/>


    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Striming Safe For Kids</title>

    <!-- Styles -->
    <link href="/css/app.css" rel="stylesheet">

    <!-- Scripts -->
    <script>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>
    </script>
</head>
<body style="background-color: #8cff35;">
    <div id="app">
      <!-- Dropdown Structure -->
<ul id="dropdown1" class="dropdown-content">
  <!-- Authentication Links -->
  @if (Auth::guest())
      <li><a href="{{ url('/login') }}">Login</a></li>
      <li><a href="{{ url('/register') }}">Register</a></li>
  @else

              <li>
                  <a href="{{ url('/logout') }}"
                      onclick="event.preventDefault();
                               document.getElementById('logout-form').submit();">
                      Logout
                  </a>
                  <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                      {{ csrf_field() }}
                  </form>
              </li>

  @endif
</ul>

  <nav>
    <div class="nav-wrapper">
      <a href="/welcome" class="brand-logo"><img src="/img/icons/smileorange.png" height="45px" weight="100px" class="img-circle" alt="User Image" /><strong>Streamin Safe For Kids</strong><img src="/img/icons/smileorange.png" height="45px" weight="100px" class="img-circle" alt="User Image" /></a>
  <?php if (!Auth::guest()): ?>
    <ul class="right hide-on-med-and-down">

      <li><form action="/video/search/" method="post" class="sidebar-form">
        {{ csrf_field() }}
              <div class="input-field" style="margin-right: 1cm;">
              <li style=""><i class="material-icons">search</i></li>
              <li><input align="middle" style= "background-color: #fff; height: 30px;"  type="text" name="q" id="q" class="validate" placeholder="Buscar..."/></li>
          </div>

      </form></li>

      <!-- Dropdown Trigger -->
      <li><a class="dropdown-button" href="#!" data-activates="dropdown1"><font size="8">{{ Auth::user()->username }}</font><i class="material-icons right">arrow_drop_down</i></a></li>

    </ul>
  <?php endif; ?>

    </div>
  </nav>
</div>
<!--<div class="col-sm-12">
  <ul id="slide-out" class="side-nav" style="transform: translateX(0px);">
    <li><div class="userView">
      <div class="background">
        <img src="img/userBG.png">
      </div>
      <a href="#!user"><img class="circle" src="img/pj.jpg"></a>
      <a href="#!name"><span class="black-text name"><strong>{{ Auth::user()->username }}</strong></span></a>
    </div></li>
    <li><a href="#!"><i class="material-icons">cloud</i>First Link With Icon</a></li>
    <li><a href="#!">Second Link</a></li>
    <li><div class="divider"></div></li>
    <li><a class="subheader">Subheader</a></li>
    <li><a class="waves-effect" href="#!">Third Link With Waves</a></li>
  </ul>
</div>-->


<section>
    <!-- Your Page Content Here -->
    @yield('main-content')
</section><!-- /.content -->

    <!-- Scripts -->
    <script src="/js/app.js"></script>
    <!--Import jQuery before materialize.js-->
    <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
    <script type="text/javascript" src="css/materialize/js/materialize.min.js"></script>
</body>
</html>
