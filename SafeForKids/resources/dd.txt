<div class="section">

        <div class="row center-align">

            <div class="col s12">

              <div class="section">

                    <div class="col s12">
                        <span class="title">Recently Added</span>
                    </div>
<div class="col s12 m6">
<div class="card medium">
  <div class="card-image waves-effect waves-block waves-light">
    <a href="https://adbeus.com/montreal/downtown/shaughnessy-cafe/">
      <img width="305" height="229" src="https://adbeus.com/wp-content/uploads/2016/10/14524533_1298243536854871_8745558600512087503_o-1-305x229.jpg" class="responsive-img wp-post-image" alt="Shaughnessy Café" title="Shaughnessy Café" srcset="https://adbeus.com/wp-content/uploads/2016/10/14524533_1298243536854871_8745558600512087503_o-1-305x229.jpg 305w, https://adbeus.com/wp-content/uploads/2016/10/14524533_1298243536854871_8745558600512087503_o-1-300x225.jpg 300w, https://adbeus.com/wp-content/uploads/2016/10/14524533_1298243536854871_8745558600512087503_o-1-768x576.jpg 768w, https://adbeus.com/wp-content/uploads/2016/10/14524533_1298243536854871_8745558600512087503_o-1-1024x768.jpg 1024w, https://adbeus.com/wp-content/uploads/2016/10/14524533_1298243536854871_8745558600512087503_o-1-800x600.jpg 800w" sizes="(max-width: 305px) 100vw, 305px">    </a>
  </div>
  <div class="card-content">
          <p class="area"><a href="https://adbeus.com/coffee/montreal/downtown/">Downtown</a></p>
        <a href="https://adbeus.com/montreal/downtown/shaughnessy-cafe/" data-deeplink="adbeus://shaughnessy-cafe"><span class="card-title activator brown-text text-darken-4">Shaughnessy Café</span></a>
  </div>
</div>
</div>

<div class="col s12 m6">
<div class="card medium">
  <div class="card-image waves-effect waves-block waves-light">
    <a href="https://adbeus.com/montreal/downtown/shaughnessy-cafe/">
      <img width="305" height="229" src="https://adbeus.com/wp-content/uploads/2016/10/14524533_1298243536854871_8745558600512087503_o-1-305x229.jpg" class="responsive-img wp-post-image" alt="Shaughnessy Café" title="Shaughnessy Café" srcset="https://adbeus.com/wp-content/uploads/2016/10/14524533_1298243536854871_8745558600512087503_o-1-305x229.jpg 305w, https://adbeus.com/wp-content/uploads/2016/10/14524533_1298243536854871_8745558600512087503_o-1-300x225.jpg 300w, https://adbeus.com/wp-content/uploads/2016/10/14524533_1298243536854871_8745558600512087503_o-1-768x576.jpg 768w, https://adbeus.com/wp-content/uploads/2016/10/14524533_1298243536854871_8745558600512087503_o-1-1024x768.jpg 1024w, https://adbeus.com/wp-content/uploads/2016/10/14524533_1298243536854871_8745558600512087503_o-1-800x600.jpg 800w" sizes="(max-width: 305px) 100vw, 305px">    </a>
  </div>
  <div class="card-content">
          <p class="area"><a href="https://adbeus.com/coffee/montreal/downtown/">Downtown</a></p>
        <a href="https://adbeus.com/montreal/downtown/shaughnessy-cafe/" data-deeplink="adbeus://shaughnessy-cafe"><span class="card-title activator brown-text text-darken-4">Shaughnessy Café</span></a>
  </div>
</div>
</div>

                </div>

            </div>

        </div>

    </div>
