@extends('layouts.auth')

@section('htmlheader_title')
    Log in
@endsection

@section('content')
<body class="hold-transition login-page" >
    <div id="app">

      <div class="contanier">
        <div id="headerwrap">
        <div class="row centered">
          <div class="col-md-12" style="background-color: #605ca8">
          <h1 style="text-align: center"> <a style=" color: black;" href="{{ url('/home') }}"><b>Streaming Safe for Kids</a></h1>
          </div>
        </div>
        </div>
        <div class="row ">
      <div class="col-md-4">
      <img class="img-responsive" height="442" width="400" src="img/welcome/He.jpg" alt="" />
    </div>
    <div class="col-md-4">

        <div class="login-box">
            <div class="login-logo">
                <h1><b>Login</h1>
            </div><!-- /.login-logo -->

        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <strong>Whoops!</strong> someproblems<br><br>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <div class="login-box-body">
        <p class="login-box-msg">  </p>
        <form action="{{ url('/login') }}" method="post">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="form-group has-feedback">
                <input type="text" class="form-control" placeholder="Nombre de usuario" name="username"/>
                <span class="glyphicon glyphicon-pencil form-control-feedback"></span>
            </div>
            <div class="form-group has-feedback">
                <input type="password" class="form-control" placeholder="Contraseña" name="password"/>
                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
            </div>
            <div class="row">
                <div class="col-xs-4">
                    <button type="submit" class="btn btn-primary btn-block btn-flat">Sing in</button>
                </div><!-- /.col -->
            </div>
        </form>
    </div><!-- /.login-box-body -->

    </div><!-- /.login-box -->
  </div>
    <div class="col-md-4">
    <img class="img-responsive" height="442" width="375" src="img/welcome/She.jpg" alt="" />
  </div>
</div>
</div>
    </div>
    @include('layouts.partials.scripts_auth')

</body>

@endsection
