@extends('layouts.app')

@section('main-content')

	<div class="container-fluid spark-screen">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">

				<div class="panel panel-default">
					<div class="panel-heading"></div>
					<?php
					$hoy=getdate();
					$nacimiento = strtotime(Auth::user()->birthdate);
					$nacimiento = date('Y',$nacimiento); ?>
					<?php $edad = $hoy['year']-$nacimiento;?>
					<?php if (isset($datos)): ?>

						<?php $playlist = $datos[0]; ?>
						<?php if (count($datos) == 3): ?>
							<?php $categorias = $datos[1]; ?>
							<?php $clips = $datos[2]; ?>
							<?php endif; ?>
							<?php if (count($datos) == 4): ?>

							<?php
								$categorias = $datos[1];
								$videosCat = $datos[2];
								$videosTag= $datos[3];
							 ?>
							 <?php endif; ?>
							 <?php if (count($datos) == 5): ?>

								 <?php
								 $categorias = $datos[1];
	 								$videosCat = $datos[2];
	 								$videosTag= $datos[3];
									$videosName= $datos[4];
	 							 ?>

						<?php endif; ?>
						<?php if (count($datos) != 1): ?>

							<?php if (isset($videosCat) && isset($videosTag) && !isset($videosName)): ?>
								<?php if (Auth::user()->restricted_mode == 'Y'): ?>
								<?php foreach ($videosCat as $videosCat): ?>
									<?php foreach ($categorias as $categoria): ?>
										<?php if ($videosCat->category_id == $categoria->id): ?>
											<?php if ($edad >= $categoria->mimimum_age): ?>
												<div class="panel-body">
													<div class="embed-responsive embed-responsive-16by9">

													<iframe class="embed-responsive-item" width="635" height="400" src="{{$videosCat->url}}" frameborder="0" allowfullscreen></iframe>
												</div>
												</div>
											<?php endif; ?>
										<?php endif; ?>
									<?php endforeach; ?>
								<?php endforeach; ?>
							<?php else: ?>
								<?php foreach ($videosCat as $videosCat): ?>
												<div class="panel-body">
													<div class="embed-responsive embed-responsive-16by9">

													<iframe class="embed-responsive-item" width="635" height="400" src="{{$videosCat->url}}" frameborder="0" allowfullscreen></iframe>
												</div>
												</div>
								<?php endforeach; ?>
							<?php endif; ?>
							<?php if (Auth::user()->restricted_mode == 'Y'): ?>
								<?php foreach ($videosTag as $videosTag): ?>
									<?php foreach ($categorias as $categoria): ?>
										<?php if ($videosTag->category_id == $categoria->id): ?>
											<?php if ($edad >= $categoria->mimimum_age): ?>
												<div class="panel-body">
													<div class="embed-responsive embed-responsive-16by9">

													<iframe class="embed-responsive-item" width="635" height="400" src="{{$videosTag->url}}" frameborder="0" allowfullscreen></iframe>
												</div>
												</div>
											<?php endif; ?>
										<?php endif; ?>
									<?php endforeach; ?>
								<?php endforeach; ?>
							<?php else: ?>
								<?php foreach ($videosTag as $videosTag): ?>
												<div class="panel-body">
													<div class="embed-responsive embed-responsive-16by9">
													<iframe class="embed-responsive-item" width="635" height="400" src="{{$videosTag->url}}" frameborder="0" allowfullscreen></iframe>
												</div>
												</div>
								<?php endforeach; ?>
							<?php endif; ?>
						<?php endif; ?>
								<?php if (isset($videosCat) && isset($videosTag) && isset($videosName)): ?>
									<?php if (Auth::user()->restricted_mode == 'Y'): ?>
									<?php foreach ($videosCat as $videosCat): ?>
										<?php foreach ($categorias as $categoria): ?>
											<?php if ($videosCat->category_id == $categoria->id): ?>
												<?php if ($edad >= $categoria->mimimum_age): ?>
													<div class="panel-body">
														<div class="embed-responsive embed-responsive-16by9">
														<iframe class="embed-responsive-item" width="635" height="400" src="{{$videosCat->url}}" frameborder="0" allowfullscreen></iframe>
													</div>
													</div>
												<?php endif; ?>
											<?php endif; ?>
										<?php endforeach; ?>
									<?php endforeach; ?>
								<?php else: ?>
									<?php foreach ($videosCat as $videosCat): ?>
													<div class="panel-body">
														<div class="embed-responsive embed-responsive-16by9">
														<iframe class="embed-responsive-item" width="635" height="400" src="{{$videosCat->url}}" frameborder="0" allowfullscreen></iframe>
													</div>
													</div>
									<?php endforeach; ?>
								<?php endif; ?>
								<?php if (Auth::user()->restricted_mode == 'Y'): ?>
									<?php foreach ($videosTag as $videosTag): ?>
										<?php foreach ($categorias as $categoria): ?>
											<?php if ($videosTag->category_id == $categoria->id): ?>
												<?php if ($edad >= $categoria->mimimum_age): ?>
													<div class="panel-body">
														<div class="embed-responsive embed-responsive-16by9">
															<iframe class="embed-responsive-item" width="635" height="400" src="{{$videosTag->url}}" frameborder="0" allowfullscreen></iframe>
														</div>
													</div>
												<?php endif; ?>
											<?php endif; ?>
										<?php endforeach; ?>
									<?php endforeach; ?>
								<?php else: ?>
									<?php foreach ($videosTag as $videosTag): ?>
													<div class="panel-body">
														<div class="embed-responsive embed-responsive-16by9">
														<iframe class="embed-responsive-item" width="635" height="400" src="{{$videosTag->url}}" frameborder="0" allowfullscreen></iframe>
													</div>
													</div>
									<?php endforeach; ?>
								<?php endif; ?>
								<?php if (Auth::user()->restricted_mode == 'Y'): ?>
									<?php foreach ($videosName as $videoName): ?>
										<?php foreach ($categorias as $categoria): ?>
											<?php if ($videoName->category_id == $categoria->id): ?>
												<?php if ($edad >= $categoria->mimimum_age): ?>
													<div class="panel-body">
														<div class="embed-responsive embed-responsive-16by9">
														<iframe class="embed-responsive-item" width="635" height="400" src="{{$videoName->url}}" frameborder="0" allowfullscreen></iframe>
													</div>
													</div>
												<?php endif; ?>
											<?php endif; ?>
										<?php endforeach; ?>
									<?php endforeach; ?>
								<?php else: ?>
									<?php foreach ($videosName as $videoName): ?>
													<div class="panel-body">
														<div class="embed-responsive embed-responsive-16by9">
														<iframe class="embed-responsive-item" width="635" height="400" src="{{$videoName->url}}" frameborder="0" allowfullscreen></iframe>
													</div>
													</div>
									<?php endforeach; ?>
								<?php endif; ?>
									<?php endif; ?>
							<?php if (isset($videos)): ?>
								<?php if (Auth::user()->restricted_mode == 'Y'): ?>
								<?php foreach ($videos as $video): ?>
									<?php foreach ($categorias as $categoria): ?>
										<?php if ($video->category_id == $categoria->id): ?>
											<?php if ($edad >= $categoria->mimimum_age): ?>
												<div class="panel-body">
													<div class="embed-responsive embed-responsive-16by9">
													<iframe class="embed-responsive-item" width="635" height="400" src="{{$video->url}}" frameborder="0" allowfullscreen></iframe>
												</div>
												</div>
											<?php endif; ?>
										<?php endif; ?>
									<?php endforeach; ?>
								<?php endforeach; ?>
							<?php else: ?>
								<?php foreach ($videos as $video): ?>
												<div class="panel-body">
													<div class="embed-responsive embed-responsive-16by9">
													<iframe class="embed-responsive-item" width="635" height="400" src="{{$video->url}}" frameborder="0" allowfullscreen></iframe>
												</div>
												</div>
								<?php endforeach; ?>
							<?php endif; ?>
							<?php endif; ?>
						<?php else: ?>
							<h1>No Hay Coinsidencias</h1>
						<?php endif; ?>
					<?php else: ?>
						<?php
		          $videos = $cosas[1];
		          $categorias = $cosas[2];
		          $playlist = $cosas[0];
		         ?>
						 <?php if (Auth::user()->restricted_mode == 'N' && Auth::user()->search == 'E'): ?>
							 <?php if (count($videos)>1): ?>
								 <?php foreach ($videos as $video): ?>
 									<?php foreach ($categorias as $categoria): ?>
 										<?php if ($video->category_id == $categoria->id): ?>
 											<?php if ($edad >= $categoria->mimimum_age): ?>
 												<div class="panel-body">
													<div class="embed-responsive embed-responsive-16by9">
 													<iframe class="embed-responsive-item" width="635" height="400" src="{{$video->url}}" frameborder="0" allowfullscreen></iframe>
												</div>
 												</div>
 											<?php endif; ?>
 										<?php endif; ?>
 									<?php endforeach; ?>
 								<?php endforeach; ?>
							 <?php else: ?>
								 <div class="panel-body">
									 <div class="embed-responsive embed-responsive-16by9">
									 <iframe class="embed-responsive-item" width="635" height="400" src="{{$videos->url}}" frameborder="0" allowfullscreen></iframe>
								 </div>
								 </div>
							 <?php endif; ?>

						<?php else: ?>
							<?php if (Auth::user()->search == 'D'): ?>
								<h1>Puedes ver los videos que eligieron para tí en la columna que esta por ahí <span class="glyphicon glyphicon-hand-left"></span> </h1>
							<?php else: ?>
								<h1>Puedes ver los videos que eligieron para tí o buscar otros videos por ahí <span class="glyphicon glyphicon-hand-left"></span> </h1>
							<?php endif; ?>

					<?php endif; ?>
				<?php endif; ?>
				</div>
			</div>
		</div>
	</div>
@endsection
