<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateListItemTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
     {
       Schema::create('list_items', function (Blueprint $table) {
           $table->increments('id');
           $table->integer('playlist_id')->unsigned();
           $table->integer('video_id')->unsigned();
           $table->timestamps();

           $table->foreign('playlist_id')->references('id')->on('playlists')->onDelete('cascade');
           $table->foreign('video_id')->references('id')->on('videos')->onDelete('cascade');
       });
     }

     /**
      * Reverse the migrations.
      *
      * @return void
      */
     public function down()
     {
         Schema::drop('list_items');
     }
}
