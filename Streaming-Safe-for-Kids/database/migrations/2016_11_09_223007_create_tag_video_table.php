<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTagVideoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
     {
       Schema::create('tags_videos', function (Blueprint $table) {
           $table->increments('id');
           $table->integer('video_id')->unsigned();
           $table->integer('tag_id')->unsigned();
           $table->timestamps();

           $table->foreign('video_id')->references('id')->on('videos')->onDelete('cascade');
           $table->foreign('tag_id')->references('id')->on('tags')->onDelete('cascade');
       });
     }

     /**
      * Reverse the migrations.
      *
      * @return void
      */
     public function down()
     {
         Schema::drop('tags_videos');
     }
}
