<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChildrenTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('childrens', function (Blueprint $table) {
          $table->increments('id');
          $table->integer('user_id')->references('id')->on('users');
          $table->string('username')->unique();
          $table->string('password');
          $table->date('birthdate');
          $table->enum('search', ['E','D']);
          $table->enum('restricted_mode', ['Y','N']);
          $table->rememberToken();
          $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('childrens');
    }
}
