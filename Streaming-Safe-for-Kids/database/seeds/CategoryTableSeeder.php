<?php

use Illuminate\Database\Seeder;

class CategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      \App\Category::create(array(
          'name'=>'T',
          'description'=> 'Apto para adolecentes',
          'mimimum_age' => 12
      ));
      \App\Category::create(array(
          'name'=>'E',
          'description'=> 'Todo publico',
          'mimimum_age' => 1
      ));
      
    }
}
