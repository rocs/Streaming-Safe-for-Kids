<?php

use Illuminate\Database\Seeder;

class TagTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      \App\Tag::create(array(
          'description'=> 'Educativo'
      ));
      \App\Tag::create(array(
          'description'=> 'Cartoons'
      ));
      \App\Tag::create(array(
          'description'=> 'Numeros'
      ));
    }
}
