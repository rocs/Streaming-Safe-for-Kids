<?php

use Illuminate\Database\Seeder;

class VideoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      \App\Video::create(array(
          'category_id'=>'2',
          'description'=> 'Corto de los volcanes de Pixar',
          'url' => "https://www.youtube.com/embed/ojJeUnqc_a8"
      ));
      \App\Video::create(array(
          'category_id'=>'1',
          'description'=> 'Corto de ajedrez de Pixar',
          'url' => "https://www.youtube.com/embed/tU25uRvbrHk"
      ));
      \App\Video::create(array(
          'category_id'=>'1',
          'description'=> 'Corto del pajarito de Pixar',
          'url' => "https://www.youtube.com/embed/U1w_JANPACc"
      ));
      \App\Video::create(array(
          'category_id'=>'2',
          'description'=> 'Corto del conejo y el mago de Pixar',
          'url' => "https://www.youtube.com/embed/CB1Pukr0nFQ"
      ));
    }
}
