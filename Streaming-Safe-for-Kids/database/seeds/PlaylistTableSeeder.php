<?php

use Illuminate\Database\Seeder;

class PlaylistTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      \App\Playlist::create(array(
          'user_id'=> '1',
          'description'=> 'Cortos de Pixar',
          'public'=> 'Y'
      ));
      \App\Playlist::create(array(
          'user_id'=> '1',
          'description'=> 'Playlist de Contribuidor',
          'public'=> 'N'
      ));
      \App\Playlist::create(array(
          'user_id'=> '2',
          'description'=> 'Playlist del padre',
          'public'=> 'N'
      ));
    }
}
