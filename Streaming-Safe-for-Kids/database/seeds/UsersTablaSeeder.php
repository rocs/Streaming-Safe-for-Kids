<?php

use Illuminate\Database\Seeder;

class UsersTablaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      \App\User::create(array(

          'name'=> 'Contrubuidor',
          'email'     => 'contributor@contributor.com',
          'password' => Hash::make('1234567'), // Hash::make() nos va generar una cadena con nuestra contraseña encriptada
          'rol' => 'C'

      ));
      \App\User::create(array(

          'name'=> 'Padre',
          'email'     => 'padre@padre.com',
          'password' => Hash::make('padre'), // Hash::make() nos va generar una cadena con nuestra contraseña encriptada
          'rol' => 'P'

      ));
    }
}
