<?php

use Illuminate\Database\Seeder;

class ChildrenPlaylistVideoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      \App\ChildrenPlaylist::create(array(
          'playlist_id'=> '1',
          'children_id'=> '1'
      ));
      \App\ChildrenPlaylist::create(array(
          'playlist_id'=> '1',
          'children_id'=> '2'
      ));
      \App\ChildrenPlaylist::create(array(
          'playlist_id'=> '1',
          'children_id'=> '3'
      ));
    }
}
