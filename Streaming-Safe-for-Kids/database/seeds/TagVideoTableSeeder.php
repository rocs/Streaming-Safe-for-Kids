<?php

use Illuminate\Database\Seeder;

class TagVideoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      \App\TagVideo::create(array(
          'tag_id'=> '1',
          'video_id'=> '1'
      ));
      \App\TagVideo::create(array(
          'tag_id'=> '2',
          'video_id'=> '2'
      ));
      \App\TagVideo::create(array(
          'tag_id'=> '3',
          'video_id'=> '3'
      ));
      \App\TagVideo::create(array(
          'tag_id'=> '1',
          'video_id'=> '4'
      ));
      \App\TagVideo::create(array(
          'tag_id'=> '2',
          'video_id'=> '1'
      ));
      \App\TagVideo::create(array(
          'tag_id'=> '3',
          'video_id'=> '2'
      ));
      \App\TagVideo::create(array(
          'tag_id'=> '1',
          'video_id'=> '3'
      ));
      \App\TagVideo::create(array(
          'tag_id'=> '2',
          'video_id'=> '4'
      ));

    }
}
