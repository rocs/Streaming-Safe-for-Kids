<?php

use Illuminate\Database\Seeder;

class ListItemVideoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      \App\ListItems::create(array(
          'playlist_id'=> '1',
          'video_id'=> '1'
      ));
      \App\ListItems::create(array(
          'playlist_id'=> '1',
          'video_id'=> '2'
      ));
      \App\ListItems::create(array(
          'playlist_id'=> '1',
          'video_id'=> '3'
      ));
      \App\ListItems::create(array(
          'playlist_id'=> '1',
          'video_id'=> '4'
      ));
      \App\ListItems::create(array(
          'playlist_id'=> '2',
          'video_id'=> '1'
      ));
      \App\ListItems::create(array(
          'playlist_id'=> '2',
          'video_id'=> '2'
      ));
      \App\ListItems::create(array(
          'playlist_id'=> '3',
          'video_id'=> '3'
      ));
      \App\ListItems::create(array(
          'playlist_id'=> '3',
          'video_id'=> '4'
      ));
    }
}
