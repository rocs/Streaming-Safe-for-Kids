<?php

use Illuminate\Database\Seeder;

class ChildrenTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      \App\Children::create(array(
          'user_id'=>'1',
          'username'=> 'nino',
          'password' => Hash::make('nino'), // Hash::make() nos va generar una cadena con nuestra contraseña encriptada
          'birthdate'=> "2000-8-2",
          'search' => 'E',
          'restricted_mode' => 'N'

      ));
      \App\Children::create(array(
          'user_id'=>'2',
          'username'=> 'nina',
          'password' => Hash::make('nina'), // Hash::make() nos va generar una cadena con nuestra contraseña encriptada
          'birthdate'=> "2010-8-2",
          'search' => 'D',
          'restricted_mode' => 'Y'

      ));
      \App\Children::create(array(
          'user_id'=>'2',
          'username'=> 'kid',
          'password' => Hash::make('kid'), // Hash::make() nos va generar una cadena con nuestra contraseña encriptada
          'birthdate'=> "2010-8-2",
          'search' => 'E',
          'restricted_mode' => 'Y'

      ));
    }
}
