<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersTablaSeeder::class);
        $this->call(ChildrenTableSeeder::class);
        $this->call(TagTableSeeder::class);
        $this->call(CategoryTableSeeder::class);
        $this->call(PlaylistTableSeeder::class);
        $this->call(VideoTableSeeder::class);
        $this->call(TagVideoTableSeeder::class);
        $this->call(ListItemVideoTableSeeder::class);
        $this->call(ChildrenPlaylistVideoTableSeeder::class);
    }
}
