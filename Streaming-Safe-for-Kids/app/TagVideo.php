<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TagVideo extends Model
{
  protected $table = "tags_videos";

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
       'video_id','tag_id',
  ];
}
