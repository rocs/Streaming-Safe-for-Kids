<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ListItems extends Model
{
  protected $table = "list_items";

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
       'playlist_id','video_id',
  ];
}
