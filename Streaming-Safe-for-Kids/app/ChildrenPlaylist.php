<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ChildrenPlaylist extends Model
{

  protected $table = "children_playlist";

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
      'children_id', 'playlist_id', 
  ];

}
